package com.example.madrentalapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction

class VehicleDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vehicle_detail)

        // argument envoyé depuis la liste :
        val vehicleToGet: Vehicle? = intent.getParcelableExtra(EXTRA_VEHICLE_DETAIL)

        // tablette :
        val fragment = VehicleFragment()

        // paramètres vers le fragment :
        val bundle = Bundle()
        bundle.putParcelable(EXTRA_VEHICLE, vehicleToGet)
        fragment.arguments = bundle

        // transaction :
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.conteneur_fragment, fragment, "fragment")
        transaction.commit()
    }
}