package com.example.madrentalapp

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso

const val EXTRA_VEHICLE = "vehicle"

class VehicleFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        return inflater.inflate(R.layout.fragment_vehicle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        val arguments = requireArguments()
        val vehicle: Vehicle? = arguments.getParcelable(EXTRA_VEHICLE)
        val tvName: TextView = view.findViewById(R.id.fragment_vehicle_name)
        val tvDetail: TextView = view.findViewById(R.id.fragment_vehicle_detail)
        val ivPic: ImageView = view.findViewById(R.id.fragment_vehicle_pic)

        tvName.text = vehicle?.nom
        val txt = "" + vehicle?.prixjournalierbase + "€/j - Cat. CO2 : " + vehicle?.categorieco2
        tvDetail.text = txt
        val imageStr = "http://s519716619.onlinehome.fr/exchange/madrental/images/" + vehicle?.image
        Picasso.get()
            .load(imageStr)
            .into(ivPic)

        val favButton: ImageButton = view.findViewById(R.id.favorite)

        favButton.setOnClickListener {
            val vehicleDTO = VehicleDTO(null, vehicle?.image, vehicle?.nom, vehicle?.prixjournalierbase, vehicle?.categorieco2)
            val nbVehicle = AppDatabaseHelper.getDatabase(this.requireContext()).vehiclesDAO().countVehiclewithName(vehicle?.nom!!)

            if (nbVehicle <= 0) {
                this.context?.let { it1 -> AppDatabaseHelper.getDatabase(it1).vehiclesDAO().insert(vehicleDTO) }
                Toast.makeText(this.context, "Vehicule ajouté aux favoris", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this.context, "Vehicule déjà mis en favoris", Toast.LENGTH_LONG).show()
            }

        }
    }

}