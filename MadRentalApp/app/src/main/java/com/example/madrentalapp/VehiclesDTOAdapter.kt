package com.example.madrentalapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class VehiclesDTOAdapter(private var listeVehicles: List<VehicleDTO>) :
    RecyclerView.Adapter<VehiclesDTOAdapter.VehicleViewHolder>()
{
    // Crée chaque vue item à afficher :
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleViewHolder
    {
        val viewVehicle = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_vehicle, parent, false)
        return VehicleViewHolder(viewVehicle)
    }

    // Renseigne le contenu de chaque vue item :
    override fun onBindViewHolder(holder: VehicleViewHolder, position: Int)
    {
        val vehiclePrixStr = listeVehicles[position].dailyPrice.toString() + "€ / jour"
        val vehicleCo2Str = "Catégorie CO2 : " + listeVehicles[position].co2cat
        val vehicleImageStr = "http://s519716619.onlinehome.fr/exchange/madrental/images/" + listeVehicles[position].pic
        holder.textViewLibelleNom.text = listeVehicles[position].name
        holder.textViewLibellePrice.text = vehiclePrixStr
        holder.textViewLibelleCo2.text = vehicleCo2Str
        Picasso.get()
            .load(vehicleImageStr)
            .into(holder.imageViewLibelleImage)
    }

    override fun getItemCount(): Int = listeVehicles.size
    // ViewHolder :
    inner class VehicleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val textViewLibelleNom: TextView = itemView.findViewById(R.id.vehicle_name)
        val textViewLibellePrice: TextView = itemView.findViewById(R.id.vehicle_price)
        val textViewLibelleCo2: TextView = itemView.findViewById(R.id.vehicle_c02)
        val imageViewLibelleImage: ImageView = itemView.findViewById(R.id.vehicle_pic)
    }
}