package com.example.madrentalapp

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Vehicle(
    val id: Int? = null,
    val nom: String? = null,
    val image: String? = null,
    val prixjournalierbase: Int? = null,
    val categorieco2: String? = null,
) : Parcelable
