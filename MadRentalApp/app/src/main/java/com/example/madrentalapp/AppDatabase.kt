package com.example.madrentalapp

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [VehicleDTO::class], version = 1)
abstract class AppDatabase : RoomDatabase()
{
    abstract fun vehiclesDAO(): VehiclesDAO
}
