package com.example.madrentalapp

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "vehicles")
class VehicleDTO(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val pic: String? = null,
    val name: String? = null,
    val dailyPrice: Int? = null,
    val co2cat: String? = null
)