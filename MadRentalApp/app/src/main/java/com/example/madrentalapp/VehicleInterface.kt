package com.example.madrentalapp

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface VehicleInterface
{
    // appel get :
    @GET("exchange/madrental/get-vehicules.php")
    fun getVehicles(): Call<List<Vehicle>>
}