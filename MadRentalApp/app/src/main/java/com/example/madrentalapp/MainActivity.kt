package com.example.madrentalapp

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

const val EXTRA_VEHICLE_DETAIL = "extraVehicleDetail"

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // vérification :
        if (!ReseauHelper.estConnecte(this))
        {
            Toast.makeText(this, "Aucune connexion internet !", Toast.LENGTH_LONG).show()
            return
        }

        val rvVehicles = findViewById<RecyclerView>(R.id.listeVehicules)
        rvVehicles.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        rvVehicles.layoutManager = layoutManager

        val service = RetrofitSingleton.retrofit.create(VehicleInterface::class.java)
        val call = service.getVehicles()
        call.enqueue(object : Callback<List<Vehicle>>
        {
            override fun onResponse(call: Call<List<Vehicle>>, response: Response<List<Vehicle>>)
            {
                if (response.isSuccessful)
                {
                    val vehicles = response.body()

                    val vehiclesAdapter = vehicles?.let { VehiclesAdapter(it, this@MainActivity) }
                    rvVehicles.adapter = vehiclesAdapter
                }
            }
            override fun onFailure(call: Call<List<Vehicle>>, t: Throwable)
            {
                Log.e("véhicule", "${t.message}")
            }
        })

        val listSwitch = findViewById<SwitchCompat>(R.id.switchFav)

        listSwitch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                // The switch enabled
                val favoriteVehicles: List<VehicleDTO> = AppDatabaseHelper.getDatabase(this).vehiclesDAO().getListeVehicles()
                val vehiclesDTOAdapter = VehiclesDTOAdapter(favoriteVehicles.toMutableList())
                rvVehicles.adapter = vehiclesDTOAdapter
            } else {
                // The switch disabled
                call.clone().enqueue(object : Callback<List<Vehicle>>
                {
                    override fun onResponse(call: Call<List<Vehicle>>, response: Response<List<Vehicle>>)
                    {
                        if (response.isSuccessful)
                        {
                            val vehicles = response.body()

                            val vehiclesAdapter = vehicles?.let { VehiclesAdapter(it, this@MainActivity) }
                            rvVehicles.adapter = vehiclesAdapter
                        }
                    }
                    override fun onFailure(call: Call<List<Vehicle>>, t: Throwable)
                    {
                        Log.e("véhicule", "${t.message}")
                    }
                })
            }
        }
    }
}