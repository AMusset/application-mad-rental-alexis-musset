package com.example.madrentalapp

import androidx.room.*

@Dao
abstract class VehiclesDAO
{
    @Query("SELECT * FROM vehicles")
    abstract fun getListeVehicles(): List<VehicleDTO>
    @Query("SELECT COUNT(*) FROM vehicles WHERE name = :nom")
    abstract fun countVehiclewithName(nom: String): Long
    @Insert
    abstract fun insert(vararg courses: VehicleDTO)
    @Update
    abstract fun update(vararg courses: VehicleDTO)
    @Delete
    abstract fun delete(vararg courses: VehicleDTO)
}
