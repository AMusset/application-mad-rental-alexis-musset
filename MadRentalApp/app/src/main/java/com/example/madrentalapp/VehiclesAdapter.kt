package com.example.madrentalapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

private const val TAG = "VehiclesAdapter"

class VehiclesAdapter(private var listeVehicles: List<Vehicle>, private val mainActivity: MainActivity) :
    RecyclerView.Adapter<VehiclesAdapter.VehicleViewHolder>()
{
    // Crée chaque vue item à afficher :
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleViewHolder
    {
        val viewVehicle = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_vehicle, parent, false)
        return VehicleViewHolder(viewVehicle)
    }

    // Renseigne le contenu de chaque vue item :
    override fun onBindViewHolder(holder: VehicleViewHolder, position: Int)
    {
        val vehiclePrixStr = listeVehicles[position].prixjournalierbase.toString() + "€ / jour"
        val vehicleCo2Str = "Catégorie CO2 : " + listeVehicles[position].categorieco2
        val vehicleImageStr = "http://s519716619.onlinehome.fr/exchange/madrental/images/" + listeVehicles[position].image
        holder.textViewLibelleNom.text = listeVehicles[position].nom
        holder.textViewLibellePrice.text = vehiclePrixStr
        holder.textViewLibelleCo2.text = vehicleCo2Str
        Picasso.get()
            .load(vehicleImageStr)
            .into(holder.imageViewLibelleImage)
    }

    override fun getItemCount(): Int = listeVehicles.size
    // ViewHolder :
    inner class VehicleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val textViewLibelleNom: TextView = itemView.findViewById(R.id.vehicle_name)
        val textViewLibellePrice: TextView = itemView.findViewById(R.id.vehicle_price)
        val textViewLibelleCo2: TextView = itemView.findViewById(R.id.vehicle_c02)
        val imageViewLibelleImage: ImageView = itemView.findViewById(R.id.vehicle_pic)

        init
        {
            itemView.setOnClickListener {
                val vehicleToGet = listeVehicles[adapterPosition]
                Log.d(TAG, "Vehicle : $vehicleToGet")

                if (mainActivity.findViewById<FrameLayout>(R.id.conteneur_fragment_FL) != null)
                {
                    // tablette :
                    val fragment = VehicleFragment()

                    // paramètres vers le fragment :
                    val bundle = Bundle()
                    bundle.putParcelable(EXTRA_VEHICLE, vehicleToGet)
                    fragment.arguments = bundle

                    // transaction :
                    val transaction: FragmentTransaction = mainActivity.supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.conteneur_fragment_FL, fragment, "fragment")
                    transaction.commit()
                }
                else
                {
                    // smartphone :
                    val intent = Intent(itemView.context, VehicleDetailActivity::class.java)
                    intent.putExtra(EXTRA_VEHICLE_DETAIL, vehicleToGet)
                    itemView.context.startActivity(intent)
                }
            }
        }
    }
}